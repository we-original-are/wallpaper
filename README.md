# wallpaper


- This program receives images from (https://api.pexels.com) using api key. The user can also download any of the images or share the url of that image or set up as their home screen image or phone lock screen.

- Due to the lack of access to the Mac system, only the settings of the Android platform are applied to the project.

*****

- این برنامه با استفاده از  api key  تصاویر را از (https://api.pexels.com) دریافت میکند. همچنین کاربر میتواند هر یک از تصاویر را دانلود کند یا url آن تصویر را share  کند و یا به عنوان تصویر صفحه اصلی یا صفحه قفل تلفن خود تنظیم کند.


- بدلیل عدم دسترسی به سیستم مک ،تنها تنظیمات پلتفرم اندروید بر روی پروژه اعمال شده است