import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:photo_view/photo_view.dart';
import 'package:share/share.dart';
import 'package:wallpaper/Global.dart';
import 'package:intl/intl.dart';
import 'package:hawk_fab_menu/hawk_fab_menu.dart';
import 'package:wallpaper_manager/wallpaper_manager.dart';

class ImageScreen extends StatefulWidget {
  const ImageScreen({Key? key, required this.index}) : super(key: key);
  final int index;

  @override
  _ImageScreenState createState() => _ImageScreenState();
}

class _ImageScreenState extends State<ImageScreen> {
  PageController pageController = PageController(initialPage: Global.index);

  String _platformVersion = 'Unknown';
  String _wallpaperFileWithCrop = 'Unknown';
  bool settingBackgroundOn = false;
  String text = 'Wallpaper HD';
  String subject = 'Wallpaper Image';
  List<String> imagePaths = [];
  bool shareOn = false;
  int _index = Global.index;
  bool loading = false;
  double progress = 0.0;
  int newProgress = 0;

  final Dio dio = Dio();

  Future<bool> saveFile(String url, String fileName) async {
    Directory directory;
    try {
      if (Platform.isAndroid) {
        if (await _requestPermission(Permission.storage)) {
          directory = (await getExternalStorageDirectory())!;
          String newPath = "";
          // /storage/emulated/0/Android/data/com.example.wallpaper/files
          List<String> folders = directory.path.split("/");
          for (int i = 1; i < folders.length; i++) {
            String folder = folders[i];
            if (folder != "Android") {
              newPath += "/" + folder;
            } else {
              break;
            }
          }
          newPath = newPath + "/wallpapersHD";
          directory = Directory(newPath);
        } else {
          return false;
        }
      } else {
        if (await _requestPermission(Permission.photos)) {
          directory = await getTemporaryDirectory();
        } else {
          return false;
        }
      }

      if (!await directory.exists()) {
        await directory.create(recursive: true);
      }
      if (await directory.exists()) {
        File saveFile = File(directory.path + "/$fileName");
        await dio.download(url, saveFile.path,
            onReceiveProgress: (downloaded, totalSize) {
          setState(() {
            progress = downloaded / totalSize;
            newProgress = (progress * 100).toInt();
          });
        });
        if (Platform.isIOS) {
          await ImageGallerySaver.saveFile(saveFile.path,
              isReturnPathOfIOS: true);
        }
        return true;
      }
    } catch (error) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Error Download file !',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }
    return false;
  }

  Future<bool> _requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      } else {
        return false;
      }
    }
  }

  downloadFile() async {
    setState(() {
      loading = true;
    });
    String timeNow = DateFormat("yyyy-MM-dd").format(DateTime.now()).toString();
    var imageLink = Global.photos[_index].src!.tiny!.split("?");

    bool downloaded = await saveFile(
        imageLink[0].toString(),
        "WallpapersHD-" +
            Global.photos[_index].id.toString() +
            Global.photos[_index].photographer.toString() +
            timeNow +
            ".jpeg");
    if (downloaded) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'downloaded successfully',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
      setState(() {
        progress = 0.0;
        newProgress = 0;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'download error!!',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
      setState(() {
        progress = 0.0;
        newProgress = 0;
      });
    }

    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    initPlatformState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Builder(
          builder: (BuildContext context) => HawkFabMenu(
              icon: AnimatedIcons.menu_close,
              fabColor: Theme.of(context).primaryColor,
              items: [
                HawkFabMenuItem(
                  label: "share image Url",
                  ontap: () {
                    setState(() {
                      var url = Global.photos[_index].src!.large!.split("?");
                      text = url[0].toString();
                      _onShareUrl(context, text);
                    });
                  },
                  icon: Icon(
                    Icons.share,
                  ),
                ),
                HawkFabMenuItem(
                  label: "set as background",
                  ontap: () {
                    mySowDialog(false);
                  },
                  icon: Icon(
                    Icons.phone_android_rounded,
                  ),
                ),
                HawkFabMenuItem(
                  label: "download",
                  ontap: () {
                    mySowDialog(true);
                  },
                  icon: Icon(
                    Icons.file_download,
                  ),
                ),
              ],
              body: Stack(
                children: [
                  PageView.builder(
                      onPageChanged: (int value) {
                        setState(() {
                          _index = value;
                        });
                      },
                      controller: pageController,
                      itemCount: Global.photos.length,
                      itemBuilder: (context, index) {
                        return Stack(
                          children: [
                            PhotoView(
                                imageProvider: NetworkImage(
                                    Global.photos[index].src!.large.toString()))
                          ],
                        );
                      }),
                  loading
                      ? Center(
                          child: newProgress == 100
                              ? Text("")
                              : downloadingDesign())
                      : Center(child: Text("")),
                  (shareOn || settingBackgroundOn)
                      ? Center(
                          child: Container(
                              padding: const EdgeInsets.all(15.0),
                              margin: const EdgeInsets.all(15.0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CircularProgressIndicator(),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  shareOn
                                      ? Text(
                                          "preparing the image\nplease wait ...",
                                          style: TextStyle(height: 1.5),
                                        )
                                      : Text(_wallpaperFileWithCrop)
                                ],
                              )),
                        )
                      : Text("")
                ],
              ))),
    );
  }

  Widget downloadingDesign() {
    double widthX = MediaQuery.of(context).size.width;
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
          color: Colors.white,
          width: widthX,
          child: loading
              ? Stack(
                  children: [
                    LinearProgressIndicator(
                      minHeight: 30,
                      value: progress,
                      backgroundColor: Theme.of(context).primaryColorLight,
                    ),
                    Container(
                      width: widthX,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Downloading : % $newProgress",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 15,
                              color: Theme.of(context).primaryIconTheme.color),
                        ),
                      ),
                    )
                  ],
                )
              : Text("")),
    );
  }

  _onShareUrl(BuildContext context, String _text) async {
    await Share.share(_text);
  }

  Future<void> initPlatformState() async {
    String platformVersion;
    try {
      platformVersion = await WallpaperManager.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  Future<void> setWallpaperFromFileWithCrop(
      int _location, String imgUrl) async {
    setState(() {
      _wallpaperFileWithCrop = "Loading Data, please wait ... ";
      settingBackgroundOn = true;
    });
    int width = Global.photos[_index].width!.toInt(),
        height = Global.photos[_index].height!.toInt();
    String result;
    var file = await DefaultCacheManager().getSingleFile(imgUrl);
    try {
      result = await WallpaperManager.setWallpaperFromFileWithCrop(
          file.path, _location, 0, 0, height, width);
    } on PlatformException {
      result = 'Failed to get wallpaper.';
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Failed to get wallpaper!!',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }
    if (!mounted) return;

    setState(() {
      settingBackgroundOn = false;
      _wallpaperFileWithCrop = result;
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Set As Background Successfully',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    });
  }

  mySowDialog(bool dialogDown) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            titlePadding: EdgeInsets.zero,
            title: dialogDown
                ? Text("")
                : Container(
                    padding: EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            topLeft: Radius.circular(10))),
                    child: Text(
                      "Options",
                      style: TextStyle(
                          color: Theme.of(context).primaryIconTheme.color),
                    )),
            content: dialogDown
                ? Text("Do you want download this photo?")
                : Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ListTile(
                        leading: Icon(Icons.phone_android_outlined),
                        title: Text("Set Home Screen"),
                        onTap: () {
                          if (_platformVersion != "Unknown") {
                            setWallpaperFromFileWithCrop(
                                WallpaperManager.HOME_SCREEN,
                                Global.photos[_index].src!.large2x.toString());
                          }
                          Navigator.pop(context);
                        },
                      ),
                      ListTile(
                        leading: Icon(Icons.lock_outlined),
                        title: Text("Set Lock Screen"),
                        onTap: () {
                          if (_platformVersion != "Unknown") {
                            setWallpaperFromFileWithCrop(
                                WallpaperManager.LOCK_SCREEN,
                                Global.photos[_index].src!.large2x.toString());
                          }
                          Navigator.pop(context);
                        },
                      ),
                      ListTile(
                        leading: Icon(Icons.app_settings_alt_outlined),
                        title: Text("Set Both Screens"),
                        onTap: () {
                          if (_platformVersion != "Unknown") {
                            setWallpaperFromFileWithCrop(
                                WallpaperManager.BOTH_SCREENS,
                                Global.photos[_index].src!.large2x.toString());
                          }
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
            actions: [
              dialogDown
                  ? TextButton(
                      onPressed: () {
                        downloadFile();
                        Navigator.pop(context);
                      },
                      child: Text("Yes"))
                  : Text(""),
              dialogDown
                  ? TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("No"))
                  : Text(""),
              dialogDown
                  ? Text("")
                  : TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Cancel"))
            ],
          );
        });
  }
}
