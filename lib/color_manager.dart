import 'package:shared_preferences/shared_preferences.dart';

class ColorManager{
  Future<void> setColorIndex(int index) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('index', index);
  }

  Future<int> getColorIndex() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int index = prefs.getInt('index')!.toInt();
    return index;
  }
}
