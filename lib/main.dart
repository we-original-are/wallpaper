import 'dart:async';
import 'package:flutter/material.dart';
import 'package:wallpaper/color_manager.dart';
import 'HomeScreen.dart';
import 'SplashScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool homeState = false;
  String title = "Wallpaper Hd";

  List<MaterialColor> _colors = <MaterialColor>[
    Colors.blue,
    Colors.purple,
    Colors.green,
    Colors.lightBlue,
    Colors.orange,
    Colors.pink,
    Colors.deepPurple,
    Colors.teal,
    Colors.yellow,
  ];

  int _index = 0;

  void replacePage() async {
    Timer(Duration(seconds: 8), () {
      setState(() {
        homeState = true;
      });
    });
  }

  @override
  void initState() {
    replacePage();
    fetchColorIndex();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      theme: ThemeData(
        primarySwatch: _colors[_index],
      ),
      home: homeState
          ? Scaffold(
              appBar: AppBar(
                title: Text(title),
                actions: [
                  IconButton(
                      onPressed: () {
                        changeColorIndex();
                      },
                      icon: Icon(Icons.remove_red_eye))
                ],
              ),
              body: Builder(builder: (BuildContext context) {
                return HomeScreen();
              }),
            )
          : SplashScreen(title: title),
    );
  }

  void changeColorIndex() async {
    setState(() {
      if (_index == _colors.length - 1) {
        _index = 0;
        ColorManager().setColorIndex(_index);
      } else {
        _index++;
        ColorManager().setColorIndex(_index);
      }
    });
  }

  void fetchColorIndex() async {
    await ColorManager().getColorIndex().then((value) {
      if (value == null) {
        setState(() {
          _index = 0;
        });
      } else {
        setState(() {
          _index = value;
        });
      }
    });
  }
}
