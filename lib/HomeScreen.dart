import 'dart:async';
import 'dart:convert' as convert;
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:wallpaper/Global.dart';
import 'package:wallpaper/ImageScreen.dart';
import 'package:wallpaper/wallpaperClass.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ScrollController _scrollController = new ScrollController();

  int page = 1;
  int perPage = 80;
  String apiKey = "563492ad6f917000010000013b35811c0de94025a9e630c82ba6f4de";

  String apiKey1 = "563492ad6f917000010000011bf1bd9b718f4b719e2d60303a7c1492";
  String apiKey2 = "563492ad6f917000010000013b35811c0de94025a9e630c82ba6f4de";

  bool errorInternet = false;

  bool connection = true;
  bool requestConnection = false;
  String timeOutMessage =
      "خطا ! برقراری ارتباط با سرور انجام نشد. زمان درخواست بیش از حد طولانی شد لطفا بعدا دوباره تلاش کنید.";

  void _getWallpaper(String page, String perPage) async {
    var url = Uri.parse(
        'https://api.pexels.com/v1/curated?page=$page&per_page=$perPage');
    await http.get(url, headers: {
      "Authorization": apiKey,
    }).then((response) {
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        setState(() {
          Global.photos += (jsonResponse["photos"] as List)
              .map((data) => Photos.fromJson(data))
              .toList();
          connection = true;
          requestConnection = false;
        });
      } else {
        print("Connection Error!");
        setState(() {
          if (apiKey == apiKey1) {
            apiKey = apiKey2;
          } else {
            apiKey = apiKey1;
          }
          connection = false;
          requestConnection = false;
        });
      }
    }).timeout(Duration(seconds: 12), onTimeout: () {
      print(timeOutMessage);
      setState(() {
        if (apiKey == apiKey1) {
          apiKey = apiKey2;
        } else {
          apiKey = apiKey1;
        }
        connection = false;
        requestConnection = false;
      });
    });
  }

  void getNewWallpaper() async {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        setState(() {
          checkTimeOutConnection();
          page += 1;
          requestConnection = true;
          _getWallpaper(page.toString(), perPage.toString());
        });
      }
    });
  }

  void checkTimeOutConnectionStartingApp() async {
    Timer(Duration(seconds: 12), () {
      if (Global.photos.length == 0) {
        setState(() {
          errorInternet = true;
        });
      }
    });
  }

  void checkTimeOutConnection() async {
    Timer(Duration(seconds: 12), () {
      if (requestConnection) {
        setState(() {
          requestConnection = false;
          connection = false;
        });
      }
    });
  }

  @override
  void initState() {
    checkTimeOutConnectionStartingApp();
    setState(() {
      page = Random.secure().nextInt(90);
      _getWallpaper(page.toString(), perPage.toString());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 5), () async {
      getNewWallpaper();
    });
    return Scaffold(
        body: Global.photos.length > 0
            ? Stack(
                children: [
                  Scrollbar(
                    child: GridView.builder(
                        padding: EdgeInsets.only(
                            left: 5, right: 5, bottom: 95, top: 5),
                        physics: BouncingScrollPhysics(),
                        itemCount: Global.photos.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 5,
                            crossAxisSpacing: 5,
                            childAspectRatio: .7),
                        controller: _scrollController,
                        itemBuilder: (context, index) {
                          return Card(
                            elevation: 10,
                            child: InkWell(
                              onTap: () {
                                Route route =
                                    MaterialPageRoute(builder: (context) {
                                  return ImageScreen(index: index);
                                });
                                Navigator.push(context, route);
                                setState(() {
                                  Global.index = index;
                                });
                              },
                              child: Image.network(
                                Global.photos[index].src!.large.toString(),
                                errorBuilder: (BuildContext context,
                                    Object exception, StackTrace? stackTrace) {
                                  return const Text('😢');
                                },
                                fit: BoxFit.scaleDown,
                              ),
                            ),
                          );
                        }),
                  ),
                  requestConnection
                      ? Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            padding: EdgeInsets.zero,
                            margin: EdgeInsets.zero,
                            height: 70,
                            child: Card(
                              color: Colors.white.withOpacity(.5),
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10))),
                              margin: EdgeInsets.zero,
                              child: Center(child: CircularProgressIndicator()),
                            ),
                          ))
                      : connection
                          ? Text("")
                          : Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(.5),
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10),
                                        topLeft: Radius.circular(10))),
                                child: Text(
                                  "Connection Error\n Please Check Internet and VPN\n Then scroll down to try again.",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ))
                ],
              )
            : Center(
                child: errorInternet
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Connection Error , Please Check Internet and VPN Then try again",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  errorInternet = false;
                                  _getWallpaper(
                                      page.toString(), perPage.toString());
                                  checkTimeOutConnectionStartingApp();
                                });
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Try again",
                                ),
                              )),
                        ],
                      )
                    : RefreshProgressIndicator(),
              ));
  }
}
